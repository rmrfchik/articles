# articles

Various articles from internetz

* [So You Want To Write Your Own CSV code?](http://thomasburette.com/blog/2014/05/25/so-you-want-to-write-your-own-CSV-code/)
* [What is a Software Architect](https://github.com/justinamiller/SoftwareArchitect)
* [Surprisingly Slow](https://gregoryszorc.com/blog/2021/04/06/surprisingly-slow/)
* [Go, Rust](https://fasterthanli.me/articles/i-want-off-mr-golangs-wild-ride) #metadata #path
